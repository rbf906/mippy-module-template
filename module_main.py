"""
Example module for MIPPY containing a single canvas and a number
of buttons that can be assigned to functions.
"""

# Import the tkinter GUI objects
from tkinter import *
from tkinter.ttk import *
import tkinter.messagebox

# You will usually need to import the classes included in
# mippy.viewing - these include MIPPYCanvas, ROI, MIPPYImage
# and other such useful things.
from mippy.viewing import *

def preload_dicom():
    """
    This method is essential for the module to run. MIPPY needs to know whether
    the module wants preloaded DICOM datasets or just paths to the files.  Most
    modules will probably want preloaded DICOM datasets so that you don't have
    to worry about different manufacturers, enhanced vs non-enhanced etc.
    However, I imagine some people will prefer to work with the raw data files
    and open the datasets themselves?
    """
    # If you want DICOM datasets pre-loaded, return True.
    # If you want paths to the files only, return False.
    # Note the capital letters on True and False.  These are important.
    return True


def flatten_series():
    """
    By default, MIPPY will pass the image datasets as a 2D list, divided by series.
    If you want only a single, flat list from all your series, return True.
    """
    return False




def execute(master_window, instance_info, images):
    """
    This is the function that will run when you run the program. If you create a GUI
    window, use "master_window" as its master.  
    
    instance_info is a dictionary object containing useful information and object
    references from MIPPY, including the image directory used, the MIPPY and
    module version numbers, and the user/temp directories on the operating system.
    Consult the MIPPY documentation for more info (https://mippy.rtfd.io)
    """

    """
    This section creates you blank GUI and sets your window title and icon.
    """

    # Create window.  All other objects will be attributes of this window.
    win = Toplevel(master_window)
    win.instance_info = instance_info
    win.title("{} {}: {}".format(win.instance_info['module_name'],win.instance_info['module_version'],win.instance_info['module_instance']))
    
    # This isn't always necessary, but if you want your loaded DICOM images or paths to be available
    # in other functions within the module, the recommended way is to make them an attribute of
    # your "win" object.  "win" can then be passed to any/all functions within the module
    # making all attributes available to that module.
    win.images = images
    
    # Create a canvas for interacting with the images, and an accompanying scrollbar
    win.im1 = MIPPYCanvas(win, width=512, height=512, drawing_enabled=True, antialias=False, use_masks=False)
    win.im1.img_scrollbar = Scrollbar(win, orient='horizontal')
    win.im1.configure_scrollbar()
    
    # Create a frame to group command buttons together, and create buttons that
    # perform functions, on the "parent" frame win.toolbar
    win.toolbar = Frame(win)
    win.button1 = Button(win.toolbar, text='Function 1', command=lambda: function1(win))
    win.button2 = Button(win.toolbar, text='Function 2', command=lambda: function2(win))
    
    # Add the buttons to the layout by placing them on the "grid" within the parent frame
    win.button1.grid(row=0, column=0, sticky='ew')
    win.button2.grid(row=1, column=0, sticky='ew')
    
    # Add an ImageFlipper object if you want one, and link it a canvas
    win.controlbox = ImageFlipper(win, win.im1)
    
    # Add a text display for results/info/errors/whatecer
    win.outputbox = Text(win, state='disabled', height=10, width=60)
    
    # Control the layout of your GUI widgets on your main ""win" object
    win.controlbox.grid(row=0, column=0, sticky='nsew')
    win.im1.grid(row=1, column=0, sticky='nsew')
    win.im1.img_scrollbar.grid(row=2, column=0, sticky='ew')
    win.toolbar.grid(row=1, column=1, rowspan=2, sticky='nsew')
    win.outputbox.grid(row=3, column=0, columnspan=2, sticky='nsew')
    
    # Assigning "weights" to each row and column of your grid controls whether or not that row/column
    # will resize when you resize the window (or can stretch beyond the size of its contents to fill
    # empty space)
    win.rowconfigure(0, weight=0)
    win.rowconfigure(1, weight=1)
    win.rowconfigure(2, weight=0)
    win.rowconfigure(3, weight=0)
    win.columnconfigure(0, weight=1)
    win.columnconfigure(1, weight=0)

    win.toolbar.columnconfigure(0, weight=1)

    # Some useful things you can do...
    # Load the images that were passed to the module on the canvas
    win.im1.load_images(images[0]) # images[0] is the first series of images passed
    win.im1.show_image(1) # You can select to display any image of the stack by default

    return


def close_window(window):
    """Closes the window passed as an argument"""
    active_frame.destroy()
    return


def output(win, txt):
    # Use this function to write to an output box win.outputbox
    # Call it like this:
    # > output(win, "This is the text I want to display")
    win.outputbox.config(state=NORMAL)
    win.outputbox.insert(END, txt + '\n')
    win.outputbox.config(state=DISABLED)
    win.outputbox.see(END)
    win.update()
    return


def clear_output(win):
    # Clears the output text box
    win.outputbox.config(state=NORMAL)
    win.outputbox.delete('1.0', END)
    win.outputbox.config(state=DISABLED)
    win.update()


def function1(win):
    # Do some stuff when you push the button
    clear_output(win)
    output(win,"Button 1 was pressed!")
    return


def function2(win):
    # Do some other stuff when you push the other button
    clear_output(win)
    output(win,"Button 2 was pressed!")
    return


def another_example_function():
    # You can do anything you want in any function that can be called within this script.
    # You can also add other .py modules to your module folder and import
    # the functions into this script.
    return