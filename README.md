# mippy-module-template

Template for GUI modules for the MIPPY software package.
Fork this repository to develop your own modules.

# Basic requirements of a MIPPY module

## module_main.py

This is the file read by MIPPY to load and launch your
module.  It MUST have the following functions:

- preload_dicom()
- flatten_series()
- execute()

## module_config.pyw

Execute this script in python (3) and complete the required
details. This will generate a "config" file for your module.
MIPPY will only list modules in its GUI for which it finds
this config file.
